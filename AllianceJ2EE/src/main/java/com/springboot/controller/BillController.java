package com.springboot.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.springboot.entities.User;
import com.springboot.service.BillService;

@Controller
@RequestMapping("/bill")
public class BillController {
	
	@Autowired
	private BillService billService;
	

//	@RequestMapping("/{namessss}/edit")
//	public String hiBill3(@PathVariable String namessss) {
//		System.out.println(" THIS IS PATH NAME - " + namessss);
//		return "bill/index";
//		return index(map);
//	}
//	@RequestMapping("/redirect")
//	public String redirect(HttpServletRequest request, ModelMap map, String locale){
//		
//		return locale;
//	}
//	
	@RequestMapping("")
	public String landing(HttpServletRequest request, ModelMap map){
		return index(request, map);
	}
	
	public boolean isLoggedIn(HttpServletRequest request){
		return null != request.getSession() && null != request.getSession().getAttribute("isLoggedIn");
	}
	
	@RequestMapping("/index")
	public String index(HttpServletRequest request, ModelMap map){
		if (!isLoggedIn(request)){		
			return login(request, map);
		}
		
		List<User> userList = billService.getUserList();
		map.addAttribute("sList", userList);
		map.addAttribute("loggedUser", request.getSession().getAttribute("loggedUser"));
		return "bill/index";	
	}
	
	@RequestMapping("/logout")
	public String logout(HttpServletRequest request){
		request.getSession().invalidate();
		return "bill/login";
	}
	
	@RequestMapping("/login")
	public String login(HttpServletRequest request, ModelMap map){
		if (isLoggedIn(request)) {
			return index(request, map);
		}	
		//String nameField = request.getParameter("nameField");
		//map.addAttribute("nameField", nameField);
		
//		map.addAttribute("errorMsg", "");
		return "bill/login";
	}
	
	@RequestMapping(value = "/authenticate", method = RequestMethod.POST)
	public String authenticate(HttpServletRequest request, ModelMap map){
		String nameField = request.getParameter("nameField");
		String passField = request.getParameter("passField");
		boolean userExists = billService.validateIfUserExists(nameField, passField);
		
		if(userExists) {
			map.addAttribute("userNameField", nameField);
			map.addAttribute("errorMsg", "");
			request.getSession().setAttribute("isLoggedIn", true);
			request.getSession().setAttribute("loggedUser", nameField);
//			return index(request, map);
		}
		map.addAttribute("errorMsg", "Invalid Credentials.");
		
		return login(request, map);
	}
	
	@RequestMapping("/signup")
	public String signup(HttpServletRequest request, ModelMap map) {	
		return "bill/signup";
	}
	
	@RequestMapping(value = "/register", method = RequestMethod.POST)
	public String register(HttpServletRequest request, ModelMap map){
		String nameField = request.getParameter("nameField");
		String passField = request.getParameter("passField");
		boolean userExists = billService.checkUserNameAvailability(nameField);
		
		map.addAttribute("signMsg", "");
		if (userExists){
			map.addAttribute("signMsg", "User already exists.");
			return signup(request, map);
		} else {
			billService.insertUser(nameField, passField);
			map.addAttribute("errorMsg", "Successfully created user: " + nameField);
			return logout(request);
		}

	}
	
	@RequestMapping("/hi")
	public String hiBill(HttpServletRequest request, ModelMap map) {
//		if (null == request.getSession() || null == request.getSession().getAttribute("isLoggedIn"))
//			return login(map, request);
		List<User> userList = billService.getUserList();
		map.addAttribute("uList", userList);

		return "bill/hi";
	}
	
//	@RequestMapping("/hi2")
//	public String hiBill2(HttpServletRequest request, ModelMap map) {
//		String nameField = request.getParameter("nameField");
//		String passField = request.getParameter("passField");
//		boolean userExists = billService.validateIfUserExists(nameField, passField);
//		
//		if(!userExists) {
//			/*
//			map.addAttribute("message", "An Intruder named [" + nameField + "] has been detected");
//			System.out.println("NAME FIELD " + nameField);
//			System.out.println("PASS FIELD " + passField);
//			*/
//			map.addAttribute("userNameField", nameField);
//			
//			return "bill/2ndjsp";
//		} else {
//			/*
//			map.addAttribute("message", "COMRADE " + nameField + " IZZA HERE");
//			System.out.println("NAME FIELD " + nameField);
//			System.out.println("PASS FIELD " + passField);
//			*/
//			map.addAttribute("userNameField", nameField);
//			
//			
//			return index(request, map);
//		}
//		
//		
//	}
	
	@RequestMapping("/{id}/edit")
	public String edit(HttpServletRequest request, ModelMap map, @PathVariable String id){
		if (!isLoggedIn(request)){		
			return login(request, map);
		}
		
		try {
			if (!billService.userIdExists(Integer.parseInt(id))){
				return "bill/hacker";
			}
		} catch (Exception e){
			return "bill/hacker";
		}
		if (!billService.userIdExists(Integer.parseInt(id))){
			return index(request, map);
		}
		
		User user = billService.getUser(Integer.parseInt(id));
		
		map.addAttribute("username", user.getUsername());
		map.addAttribute("password", user.getPassword());
		
		map.addAttribute("userID", id);		
		
		return "bill/edit";
	}
	
	@RequestMapping(value = "/{id}/update", method = RequestMethod.POST)
	public String updateUser(ModelMap map, HttpServletRequest request, @PathVariable String id) {
//		if (null == request.getSession() || null == request.getSession().getAttribute("isLoggedIn"))
//			return login(map, request);
//		String[] userIdsToDelete = request.getParameterValues("userId");
//		if (null != userIdsToDelete && userIdsToDelete.length > 0) {
//			billService.deleteStudents(userIdsToDelete);
//		} else {
		if (!billService.userIdExists(Integer.parseInt(id))){
			return "bill/hacker";
		}
		
		String newUsername = request.getParameter("nameField");
		String newPassword = request.getParameter("passField");
		User user = billService.getUser(Integer.parseInt(id));
		
		boolean passwordFlag = false;
		boolean usernameFlag = false;
		
		if (null != newPassword && newPassword.trim().length() > 0){
			if (!newPassword.equals(user.getPassword())){
				passwordFlag = true;
			}

			billService.updatePassword(Integer.parseInt(id), newPassword);
		}
		
		if (null != newUsername && newUsername.trim().length() > 0){
			if (!billService.checkUserNameAvailability(newUsername) || newUsername.equals(user.getUsername())){
				if (!newUsername.equals(user.getUsername())){
					usernameFlag = true;
				}
				
				map.addAttribute("userError", "");
				billService.updateUsername(Integer.parseInt(id), newUsername);
			} else {
				map.addAttribute("userError", "Username is taken.");
			}
		}
		
		if (passwordFlag && usernameFlag){
			map.addAttribute("updateMsg", "User Name and Password updated.");
		} else if (passwordFlag && !usernameFlag) {
			map.addAttribute("updateMsg", "Password updated.");
		} else if (usernameFlag && !passwordFlag) {
			map.addAttribute("updateMsg", "User Name updated.");
		} else {
			map.addAttribute("updateMsg", "");
		}
		

//		userToSave.setPassword(password);
//		
//		if (null != username && username.length() > 0) {
//			billService.updatePass(userToSave);
//			System.out.println("UPDATE PASSWORD");
//		} else {
//			billService.insertUser(username, newPassword);
//		}
		return edit(request, map, id);

	}
	
	@RequestMapping(value = "/{id}/delete", method = RequestMethod.POST)
	public String saveUser(HttpServletRequest request, ModelMap map, @PathVariable String id){
//		String userToDelete = request.getParameter("userId");
		billService.deleteUser(Integer.parseInt(id));		
		return index(request, map);
	}
		
//	@RequestMapping("/register")
//	public String Createuser(){
//		return "bill/register";
//	}

}