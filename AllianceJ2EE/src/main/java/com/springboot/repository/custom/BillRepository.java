package com.springboot.repository.custom;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;

import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.springframework.stereotype.Repository;

import com.springboot.entities.User;

@Repository
@Transactional
public class BillRepository {
	
	@PersistenceContext
	private EntityManager em;
	
	public boolean userExists(String username, String password) {
		boolean exists = false;
		
		Session session = em.unwrap(Session.class);
		StringBuilder stringQuery = new StringBuilder("SELECT * FROM users WHERE username = :username AND password = :password");
		SQLQuery query = session.createSQLQuery(stringQuery.toString());
		query.setParameter("username", username);
		query.setParameter("password", password);
		List list = query.list();
//		exists = (list != null && list.size() > 0);
		
		if(list != null && list.size() > 0) {
			exists = true;
		}
	
		return exists;
	}
	
	public boolean userNameExists(String username) {
		
		
		Session session = em.unwrap(Session.class);
		StringBuilder stringQuery = new StringBuilder("SELECT * FROM users WHERE username = :username");
		SQLQuery query = session.createSQLQuery(stringQuery.toString());
		query.setParameter("username", username);
		List list = query.list();
		
		boolean exists = list != null && list.size() > 0;
		return exists;
	}
	
	public void insertUser(String username, String password){
		User u = new User();
		u.setUsername(username);
		u.setPassword(password);
		
		try{
			em.persist(u);
		}catch(Exception e){
			System.out.println("Something happened");
		}
	}
	
	public void deleteUsers(int userToDelete) {
//	public void deleteUsers(EntityManager em, int userToDelete) {
		Query userQuery = em.createQuery("DELETE From User where userID = :userToDelete");
		userQuery.setParameter("userToDelete", userToDelete);
		userQuery.executeUpdate();
	}
			
	public User getUserByUsername(String username)
	{
		User user = null;
		try {
			Query userQuery = em.createQuery("From User u where u.username = :username");
			userQuery.setParameter("username", username);
			user = (User) userQuery.setMaxResults(1).getSingleResult();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return user;
	}
	
	public void updateUser(User user){
		em.merge(user);
	}

	public List<User> getUsers() {
		List<User> userList = new ArrayList<User>();
		Query userQuery = em.createQuery("From User");

		userList = userQuery.getResultList();

		return userList;
	}

	public User getUserById(int userID) {
		User user = null;
		try {
			Query userQuery = em.createQuery("From User u where u.userID = :userID");
			userQuery.setParameter("userID", userID);
			user = (User) userQuery.setMaxResults(1).getSingleResult();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return user;
	}
}
	

