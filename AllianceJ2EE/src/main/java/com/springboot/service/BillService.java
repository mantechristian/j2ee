package com.springboot.service;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.springboot.entities.User;
import com.springboot.repository.custom.BillRepository;

@Service
public class BillService {
	@PersistenceContext
	private EntityManager em;
	
	@Autowired
	private BillRepository billRepository;
	
	public boolean validateIfUserExists(String username, String password) {
//		boolean exists = false;
//		if(username != null && username.length() > 5) {
//			exists = true;
//		}
		boolean exists = billRepository.userExists(username, password);
		return exists;
	}
	
	public boolean checkUserNameAvailability(String username) {
		boolean exists = billRepository.userNameExists(username);
		return exists;
	}
	
	public void insertUser(String username, String password){
		billRepository.insertUser(username, password);
	}
	
	public boolean userIdExists(int id){
		return !(null == billRepository.getUserById(id));
	}
	
	public void updatePassword(int id, String newPassword) {
		// Retrieve the student to be updated
		User user = billRepository.getUserById(id);
		
		user.setPassword(newPassword);
		billRepository.updateUser(user);
		// Save the update
	}
	
	public void updateUsername(int id, String newUsername) {
		// Retrieve the student to be updated
		User user = billRepository.getUserById(id);
		
		user.setUsername(newUsername);
		billRepository.updateUser(user);
		// Save the update
	}
	
	public User getUser(int id){
		return billRepository.getUserById(id);
	}

	public List<User> getUserList() {
		return billRepository.getUsers();
	}
	
	public void deleteUser(int userToDelete) {
//	public void deleteUser(EntityManger em, int userToDelete) {
//		billRepository.deleteUsers(em,userToDelete);
		billRepository.deleteUsers(userToDelete);
	}
}
