<!DOCTYPE html>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html lang="en">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
<head>
</head>
<style>
body{
  background-color: #5B8A99;
}
.mainnavbar{
  background-color:transparent;
}
a{
  color:white;
}
.loginpage{
  width:30%;
  margin-right: 130px;
  margin-top: 100px;
  float:center;
 
}
.brandlogo{
  color:white;
  font-size: 30px;
}
.namelogo{
  color:#5B8A99;
}
h2{
  color:white;
}
a{
color:white;
}
a:hover{
	text-decoration:none;
	color:black;
}
</style>
<body>
<nav class="navbar navbar-light bg-danger">
  <h3><span class="bagde badge-danger">Alliance Training</span></h3>
  <a href="http://localhost:8010/bill/login">Already have an account? Login.</a>
</nav>
<!--content!-->
<center>
<div class="loginpage">
<form class="form-signin" method="POST" action="http://localhost:8010/bill/register">
        <h2 class="form-signin-heading text-center">Create an account</h2>
        <label><span class="badge badge-danger">${signMsg}</span></label>
        <label for="inputEmail" class="sr-only">Username</label>
        <input type="text" id="inputEmail" name="nameField" class="form-control" placeholder="Username" required autofocus><br>
        <label for="inputPassword" class="sr-only">Password</label>
        <input type="password" id="inputPassword" name="passField" class="form-control" placeholder="Password" required><br>
        <button class="btn btn-lg btn-info btn-block" type="submit">Create</button><br/>
        
      </form>
</div>
</body>
</html>