<!DOCTYPE html>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html lang="en">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
<head>
</head>
<style>
body{
  background-color: #5B8A99;
}
.mainnavbar{
  background-color:transparent;
}
a{
  color:white;
}
.loginpage{
  width:50%;
  margin-right: 130px;
  margin-top: 100px;
  float:center;
 
}
.brandlogo{
  color:white;
  font-size: 30px;
}

h2{
  color:white;
}
.error{
color:
}
a{
color:white;
}
a:hover{
	text-decoration:none;
	color:black;
}
ul li {
text-decoration:none;
display:inline-block;
}
</style>
<body>

<nav class="navbar navbar-light bg-danger">
  <h3><span class="bagde badge-danger">Alliance Training</span></h3>
  <ul style="float:right">
  <li><label style="font-size:20px; color:white"><b>Welcome, Alliance!</b></label></li>&nbsp;
  <li><a href="http://localhost:8010/bill/logout" style="font-size:20px"> Logout</a></li>
  </ul>
</nav>
<br/>
<center>
<h1>List of Users</h1>
<table class="table table-bordered" style="width:70%">
   <thead class="thead-dark">
    <tr>
      <th scope="col">User ID</th>
      <th scope="col">User Name</th>
      <th scope="col">Password</th>
      <th scope="col" style="width:250px">Actions</th>
    </tr>
  </thead>

  <tbody>
  <c:forEach items="${sList}" var="entry">
    <tr>
      <th scope="row">${entry.userID}</th>
      <td>${entry.username}</td>
      <td>${entry.password}</td>
      <td> 

      <ul style="display:inline-block">
      <li><form method="post" action="http://localhost:8010/bill/${entry.userID}/edit">
      <input type="text" name="userid" value="${entry.userID }" hidden/>
      <button type="submit" class="btn btn-info" style="font-size:20px">Edit</button>&nbsp;
      </form></li>
      <li><form method="POST" action="http://localhost:8010/bill/${entry.userID}/delete">
       <input type="text" name="userId" value="${entry.userID }" hidden/>
      <button type="submit" class="btn btn-danger" style="font-size:20px">Delete</button>
      </li>
      </form>
      </ul>
<!--
      <a href="http://localhost:8010/bill/${entry.userID}/edit" style="font-size:20px"><span class="badge badge-info">Edit</a>&nbsp;
      <a href="#" style="font-size:20px"><span class="badge badge-danger">Delete</a>
 -->
      </td>
    </tr>
    </c:forEach>
  </tbody>
 
</table>

<!-- 

Welcome, ${userNameField}<br>
<form method="post" action="http://localhost:8010/bill/save">
	<input type="hidden" name="userNameField" value="${userNameField}"/><br/>
-->
		<!--Pass : <input type="password" name="passField"/><br/>-->
<!--
		New Pass: <input type="password" name="newPassField"/><br/>
	<button type="submit">Submit</button>
</form>>

 -->
 
<br>

</body>
</html>